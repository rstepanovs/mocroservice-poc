package lv.romans.master.auth.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import lv.romans.master.auth.user.User
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.io.BufferedReader
import java.security.PublicKey
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthenticationFilter(private val algorithm: Algorithm, authManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    init {
        authenticationManager = authManager
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        val creds = request.inputStream.bufferedReader().readText()
        val userCredentials = jacksonObjectMapper().readValue(creds, Credentials::class.java)
        println(userCredentials)
        return authenticationManager.authenticate(UsernamePasswordAuthenticationToken(userCredentials.email, userCredentials.password))
    }

    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        val user = authResult.principal as User
        val token = JWT.create().withSubject(user.username)
                .withClaim("role", user.role.name)
                .sign(algorithm)
        response.addHeader("Authorization", "Bearer $token");
    }
}

data class Credentials(val email: String, val password: String)