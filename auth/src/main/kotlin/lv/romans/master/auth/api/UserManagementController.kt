package lv.romans.master.auth.api

import lv.romans.master.auth.user.UserService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class UserManagementController(val userService: UserService) {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("create")
    fun register(@RequestBody registerRequest: RegisterRequest) {
        userService.createUser(registerRequest)
    }

    data class RegisterRequest(val email: String, val password: String)

}