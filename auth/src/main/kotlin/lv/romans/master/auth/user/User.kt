package lv.romans.master.auth.user

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*

@Entity
data class User(@Id @GeneratedValue val id: Long, @Column(unique = true) val email: String, val pass: String, @Enumerated(EnumType.STRING) val role: UserRole): UserDetails {
    override fun getPassword() = pass
    override fun getUsername() = email
    override fun getAuthorities() = listOf(SimpleGrantedAuthority(role.name))

    override fun isAccountNonExpired() = true
    override fun isAccountNonLocked() = true
    override fun isCredentialsNonExpired() = true
    override fun isEnabled() = true
}

enum class UserRole {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN
}