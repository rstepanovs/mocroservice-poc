package lv.romans.master.auth

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.bouncycastle.util.encoders.Base64
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.security.KeyFactory
import java.security.interfaces.RSAPrivateKey
import java.security.spec.PKCS8EncodedKeySpec

@EnableDiscoveryClient
@SpringBootApplication
class AuthApplication {

    @Bean
    fun privateKey(): RSAPrivateKey {
        val privateCertificate = ClassPathResource("security/private.pem").file
        val privateKey = privateCertificate.readText()
                .replace("-----BEGIN PRIVATE KEY-----\n", "")
                .replace("\n-----END PRIVATE KEY-----", "")
                .run { Base64.decode(this) }
        val keySpec = PKCS8EncodedKeySpec(privateKey)
        return KeyFactory.getInstance("RSA").generatePrivate(keySpec) as RSAPrivateKey
    }

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder()

    @Bean
    fun signingAlgorithm(signingKey: RSAPrivateKey): Algorithm {
        return Algorithm.RSA256(null, signingKey)
    }
}

fun main(args: Array<String>) {
    runApplication<AuthApplication>(*args)
}
