package lv.romans.master.auth.user

import lv.romans.master.auth.api.UserExistsException
import lv.romans.master.auth.api.UserManagementController
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.sql.SQLIntegrityConstraintViolationException

@Service
class UserService(val repository: UserRepository, val encoder: PasswordEncoder): UserDetailsService {
    override fun loadUserByUsername(email: String): UserDetails {
        val user = repository.findByEmail(email)
        println(user)
        return user!!
    }

    fun createUser(registration: UserManagementController.RegisterRequest) {
        try {
            repository.save(User(0, registration.email, encoder.encode(registration.password), UserRole.ROLE_USER))
        } catch (e: DataIntegrityViolationException) {
            throw UserExistsException
        }
    }
}