import io.restassured.RestAssured
import io.restassured.http.ContentType
import java.util.*

class Authorization(val email: String = "${UUID.randomUUID().toString().substring(0, 5)}@gmail.lv", val password: String = "123455") {

    var bearerToken: String? = null
        get() = field
        private set;
    fun createUser(): Authorization {
        RestAssured.given()
            .contentType(ContentType.JSON)
            .body(Register(email, password))
            .`when`()
            .post("http://localhost:6666/api/auth/create")
            .andReturn()
        return this
    }


    fun login() {
        val response = RestAssured.given()
            .contentType(ContentType.JSON)
            .body(Register(email, password))
            .`when`()
            .post("http://localhost:6666/api/auth/login")
            .andReturn()
        bearerToken = response.header("Authorization")
    }
}

data class Register(val email: String, val password: String)
