import io.restassured.response.ResponseOptions
import io.restassured.specification.RequestSpecification

fun RequestSpecification.addBearer(token: String): RequestSpecification {
    return this.header("Authorization", token)
}