import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.response.ValidatableResponse
import org.junit.Test

class AuthTest {

    @Test
    fun `creating user is successful`() {
        val email = "test@test.com"
        val password = "1234"
        register(email, password)
            .statusCode(201)
    }

    @Test
    fun `creating user with duplicate email fails`() {
        register("test1@test.com", "1234").statusCode(201)
        register("test1@test.com", "1234").statusCode(409)
    }

    @Test
    fun `created user receives jwt on login`() {
        register("test3@test.com", "1234").statusCode(201)
        login("test3@test.com", "1234").statusCode(200).and().log().all(true)
    }

    private fun register(email: String, password: String): ValidatableResponse {
        return RestAssured.given()
            .contentType(ContentType.JSON)
            .body(Register(email, password))
            .`when`()
            .post("http://localhost:6666/api/auth/create")
            .then()
            .assertThat()
    }

    private fun login(email: String, password: String): ValidatableResponse {
        return RestAssured.given()
            .contentType(ContentType.JSON)
            .body(Register(email, password))
            .`when`()
            .post("http://localhost:6666/api/auth/login")
            .then()
            .assertThat()
    }


    data class Register(val email: String, val password: String)
}