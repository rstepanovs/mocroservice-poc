import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.equalTo
import org.junit.Ignore
import org.junit.Test
import java.util.*
import kotlin.math.ceil
import kotlin.random.Random

class BuildingCreationTest {

    @Test
    fun `new building is added with apartments`() {
        val auth = Authorization()
        auth.createUser().login()
        val token = auth.bearerToken!!
        val building = createBuilding(token)
        val apartment = Apartment(1, "asfg")
        val apartments = listOf(apartment)
        addApartments(token, building.id, apartments)

    }

    @Test
    fun `extract user addresses`() {
        val auth = Authorization()
        auth.createUser().login()
        val email = auth.email
        val token = auth.bearerToken!!
        val building1 = createBuilding(token)
        val building2 = createBuilding(token)

        val apartments1 = (1..10).map { Apartment(it, if (true) email else "another@gmail.com") }
        val apartments2 = (1..10).map { Apartment(it, if (true) email else "another@gmail.com") }
        addApartments(token, building1.id, apartments1)
        addApartments(token, building2.id, apartments2)

        val response = given()
            .contentType(ContentType.JSON)
            .addBearer(token)
            .`when`()
            .get("http://localhost:6666/api/apartments/my")
            .thenReturn().body().`as`<MyApartmentsSummaryResponse>(MyApartmentsSummaryResponse::class.java) as MyApartmentsSummaryResponse

        val expected = MyApartmentsSummaryResponse(
            apartments1.filter { it.ownerEmail == email }.map { ApartmentInfoDto(it.number, building1.address) }
                    +
                    apartments2.filter { it.ownerEmail == email }.map { ApartmentInfoDto(it.number, building2.address) }
        )

        assertThat(response.apartments, equalTo(expected.apartments))
    }

    private fun addApartments(token: String, buildingId: Long, apartments: List<Apartment>) {
        given()
            .contentType(ContentType.JSON)
            .body(AddApartments(buildingId, apartments))
            .addBearer(token)
            .`when`()
            .patch("http://localhost:6666/api/buildings/apartment")
            .then()
            .assertThat()
            .statusCode(200)
    }


    fun createBuilding(token: String, address: Address = Address(), managerEmail: String = "romans@stepanovs.com"): Building {
        val id = given()
            .contentType(ContentType.JSON)
            .body(AddBuilding(address, managerEmail))
            .addBearer(token)
            .`when`()
            .post("http://localhost:6666/api/buildings/add")
            .thenReturn().body().print().toLong()
        return Building(id, address)
    }


    data class Building(val id: Long, val address: Address)

    data class Address(
        val city: String = "Riga",
        val street: String = "usmas",
        val streetNum: Int = Random.nextInt(100)
    )

    data class AddBuilding(val address: Address, val managerEmail: String)

    data class Apartment(val number: Int, val ownerEmail: String)

    data class AddApartments(val buildingId: Long, val apartments: List<Apartment>)


    data class MyApartmentsSummaryResponse(val apartments: List<ApartmentInfoDto>)

    data class ApartmentInfoDto(val number: Int, val address: Address?)
}