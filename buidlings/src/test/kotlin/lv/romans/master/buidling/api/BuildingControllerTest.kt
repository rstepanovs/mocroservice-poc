package lv.romans.master.buidling.api

import lv.romans.master.buidling.data.BuildingService
import lv.romans.master.buidling.dto.AddApartmentsRequest
import lv.romans.master.buidling.dto.AddBuildingRequest
import lv.romans.master.buidling.dto.AddressDto
import lv.romans.master.buidling.dto.ApartmentDto
import lv.romans.master.buidling.toJson
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension::class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
@Import(BuildingService::class)
class BuildingControllerTest {

    @Autowired
    private lateinit var mvc: MockMvc

    @Test
    fun `adding new building finishes correctly`() {
        val building = createNewBuilding()
        val mockServerHttpRequest = MockMvcRequestBuilders.post("/add").contentType(MediaType.APPLICATION_JSON).content(building.toJson())

        mvc.perform(mockServerHttpRequest)
                .andExpect(status().isCreated)
    }

    @Test
    fun `duplicate entry cannot be added`() {
        val building = createNewBuilding()
        val mockServerHttpRequest = MockMvcRequestBuilders.post("/add").contentType(MediaType.APPLICATION_JSON).content(building.toJson())

        mvc.perform(mockServerHttpRequest).andReturn()
        mvc.perform(mockServerHttpRequest)
                .andExpect(status().isConflict)
    }


    @Test
    fun `adding new apartments is successful`() {
        val building = createNewBuilding()
        val mockServerHttpRequest = MockMvcRequestBuilders.post("/add").contentType(MediaType.APPLICATION_JSON).content(building.toJson())

        val createdBuilding = mvc.perform(mockServerHttpRequest)
                .andExpect(status().isCreated)
                .andReturn()
        val buildingId = createdBuilding.response.contentAsString.toLong()

        val listOfApartments = (1..10).map { ApartmentDto(it, UUID.randomUUID().toString()) }

        val addApartments = MockMvcRequestBuilders
                .patch("/apartments").contentType(MediaType.APPLICATION_JSON)
                .content(AddApartmentsRequest(buildingId, listOfApartments).toJson())

        mvc.perform(addApartments).andExpect { status().is2xxSuccessful }
    }


    private fun createNewBuilding(): AddBuildingRequest {
        val address = AddressDto("Riga", "Usmas", 19)
        val managerEmail = "test@testeasyrhbv.com"
        val building = AddBuildingRequest(address, managerEmail)
        return building
    }

}