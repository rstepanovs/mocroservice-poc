package lv.romans.master.buidling.data

import lv.romans.master.buidling.api.BuildingNotFound
import lv.romans.master.buidling.dto.AddApartmentsRequest
import lv.romans.master.buidling.dto.AddressDto
import lv.romans.master.buidling.dto.ApartmentDto
import lv.romans.master.buidling.ext.ApartmentApi
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations.initMocks
import java.util.*

internal class BuildingServiceTest {
    @Mock
    private lateinit var repository: BuildingRepository
    @Mock
    private lateinit var api: ApartmentApi

    val subject by lazy { BuildingService(repository, api)}

    @BeforeEach
    internal fun setUp() {
        initMocks(this)
    }

    @Test
    fun `adding apartment for non-existent building throws error`() {
        val buildingId = 5L
        given(repository.findById(buildingId)).willReturn(Optional.empty())

        assertThrows(BuildingNotFound::class.java) { subject.addApartment(AddApartmentsRequest(buildingId, listOf())) }
    }

    @Test
    fun `adding apartment calls external api`() {
        val buildingId = 5L
        val building = Building(5, Address(1, "sad", "wqe", 33), "qweqw")
        given(repository.findById(buildingId)).willReturn(Optional.of(building))

        val apartments = listOf(ApartmentDto(1, "ujyh"))
        subject.addApartment(AddApartmentsRequest(buildingId, apartments))

        Mockito.verify(api).addApartments(ApartmentApi.AddApartments(buildingId, apartments))
    }
}