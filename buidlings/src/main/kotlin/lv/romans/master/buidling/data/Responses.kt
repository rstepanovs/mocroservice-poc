package lv.romans.master.buidling.data


data class BuildingAddressResponse(val addresses: List<BuildingAddress>)

data class BuildingAddress(val id: Long, val city: String, val street: String, val streetNum: Int)