package lv.romans.master.buidling.api

import lv.romans.master.buidling.data.BuildingAddressResponse
import lv.romans.master.buidling.data.BuildingService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("internal")
class InternalController(val service: BuildingService) {

    @GetMapping("/addresses")
    @ResponseStatus(HttpStatus.OK)
    fun buildingsForAddresses(@RequestParam("buildings") ids: Set<Long>): BuildingAddressResponse {
        return BuildingAddressResponse(service.getAddresses(ids))
    }

}