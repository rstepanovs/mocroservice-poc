package lv.romans.master.buidling.dto

import lv.romans.master.buidling.data.Address

data class ApartmentDto(val number: Int, val ownerEmail: String)

data class AddressDto(val city: String,
                      val street: String,
                      val streetNum: Int) {

    companion object {
        fun fromEntity(address: Address) = AddressDto(address.city, address.street, address.streetNum)
    }
}