package lv.romans.master.buidling.data

import feign.FeignException
import lv.romans.master.buidling.api.AddApartmentFailure
import lv.romans.master.buidling.api.BuildingForAddressExists
import lv.romans.master.buidling.api.BuildingNotFound
import lv.romans.master.buidling.dto.AddApartmentsRequest
import lv.romans.master.buidling.dto.AddBuildingRequest
import lv.romans.master.buidling.ext.ApartmentApi
import org.springframework.stereotype.Service

@Service
class BuildingService(private val repository: BuildingRepository, private val apartmentApi: ApartmentApi) {

    fun addBuilding(request: AddBuildingRequest): Long {
        val addressDto = request.address
        val existingBuilding = repository.findByAddress_CityAndAddress_StreetAndAddress_StreetNum(addressDto.city, addressDto.street, addressDto.streetNum)
        if (existingBuilding != null) {
            throw BuildingForAddressExists
        }
        return repository.save(Building(0, Address(0, addressDto.city, addressDto.street, addressDto.streetNum), request.managerEmail)).id
    }

    fun addApartment(request: AddApartmentsRequest) {
        val exists = repository.existsById(request.buildingId)
        if (exists.not()) throw BuildingNotFound
        try {
            apartmentApi.addApartments(ApartmentApi.AddApartments(request.buildingId, request.apartments))
        } catch (e: FeignException) {
            throw AddApartmentFailure(e.status(), e.message)
        }
    }

    fun getAddresses(ids: Set<Long>): List<BuildingAddress> {
        return repository.findAllById(ids).map { BuildingAddress(it.id, it.address.city, it.address.street, it.address.streetNum) }
    }


}