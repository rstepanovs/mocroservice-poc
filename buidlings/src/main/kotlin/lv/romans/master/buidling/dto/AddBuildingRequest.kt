package lv.romans.master.buidling.dto

data class AddBuildingRequest(val address: AddressDto, val managerEmail: String)

data class AddApartmentsRequest(val buildingId: Long, val apartments: List<ApartmentDto>)
