package lv.romans.master.buidling.ext

import lv.romans.master.buidling.dto.ApartmentDto
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PostMapping

@FeignClient("apartment-service")
interface ApartmentApi {

    @PostMapping("/add")
    fun addApartments(addApartmentsRequest: AddApartments)

    data class AddApartments(val buildingId: Long, val apartments: List<ApartmentDto>)
}