package lv.romans.master.buidling.data

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface BuildingRepository: CrudRepository<Building, Long> {

    fun findByAddress_CityAndAddress_StreetAndAddress_StreetNum(city: String, street: String, streetNumber: Int): Building?
}