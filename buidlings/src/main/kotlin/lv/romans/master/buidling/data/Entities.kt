package lv.romans.master.buidling.data

import javax.persistence.*

@Entity
class Building(@Id @GeneratedValue val id: Long,
               @OneToOne(cascade = [CascadeType.ALL]) val address: Address,
               val managerEmail: String
)

@Entity
class Address(@Id @GeneratedValue val id: Long,
              val city: String,
              val street: String,
              val streetNum: Int
)