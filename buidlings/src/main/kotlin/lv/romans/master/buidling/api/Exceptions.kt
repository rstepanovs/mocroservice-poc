package lv.romans.master.buidling.api

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Building already exists")
object BuildingForAddressExists : Exception()

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Building not found")
object BuildingNotFound : Exception()

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Building not found")
class AddApartmentFailure(val statusCode: Int, override val message: String?) : Exception()