package lv.romans.master.buidling.api

import lv.romans.master.buidling.data.BuildingService
import lv.romans.master.buidling.dto.AddApartmentsRequest
import lv.romans.master.buidling.dto.AddBuildingRequest
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class BuildingController(val service: BuildingService) {

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    fun addBuilding(@RequestBody request: AddBuildingRequest): Long {
        return service.addBuilding(request)
    }

    @RequestMapping(method = [RequestMethod.PATCH], value = ["/apartment"])
    @ResponseStatus(HttpStatus.OK)
    fun addApartment(@RequestBody request: AddApartmentsRequest) {
        service.addApartment(request)
    }
}