package lv.romans.master.apartment.controllers

import lv.romans.master.apartment.fromJson
import lv.romans.master.apartment.service.AddApartmentsRequest
import lv.romans.master.apartment.service.ApartmentDto
import lv.romans.master.apartment.service.MyApartmentsSummaryResponse
import lv.romans.master.apartment.toJson
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
internal class ApartmentControllerTest {

    @Autowired
    lateinit var mvc: MockMvc
    @Test
    fun `adding multiple apartments with one owner`() {
        val email = "romans@stepanovs.com"
        val list1 = listOf(
                ApartmentDto(1, email),
                ApartmentDto(2, email),
                ApartmentDto(5, "adasd@dd.c")
        )
        val list2 = listOf(
                ApartmentDto(3, email),
                ApartmentDto(88, email),
                ApartmentDto(12, "xaasd@dd.c")
        )

        mvc.perform(MockMvcRequestBuilders.post("/add").content(AddApartmentsRequest(1, list1).toJson())).andReturn()
        mvc.perform(MockMvcRequestBuilders.post("/add").content(AddApartmentsRequest(2, list2).toJson())).andReturn()

        val result = MockMvcRequestBuilders.get("/my").header("x-user-email", email).let { mvc.perform(it).andReturn() }

        val myApartments: MyApartmentsSummaryResponse = result.response.contentAsString.fromJson()
    }

}