package lv.romans.master.apartment

import com.fasterxml.jackson.databind.ObjectMapper

fun Any.toJson() = ObjectMapper().writeValueAsString(this)

inline fun <reified T> String.fromJson() = ObjectMapper().readValue(this, T::class.java)