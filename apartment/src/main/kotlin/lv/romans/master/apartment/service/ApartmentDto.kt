package lv.romans.master.apartment.service

data class ApartmentDto(val number: Int, val ownerEmail: String)

data class ApartmentInfoDto(val number: Int, val address: AddressDto?)

data class AddressDto(val city: String, val street: String, val streetNum: Int)

