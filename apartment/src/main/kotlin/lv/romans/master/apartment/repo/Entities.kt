package lv.romans.master.apartment.repo

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Apartment(@Id @GeneratedValue val id: Long, val buildingId: Long, val number: Int, val ownerEmail: String)