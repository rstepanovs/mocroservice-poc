package lv.romans.master.apartment.controllers

import com.netflix.ribbon.proxy.annotation.Http
import lv.romans.master.apartment.service.AddApartmentsRequest
import lv.romans.master.apartment.service.ApartmentService
import lv.romans.master.apartment.service.MyApartmentsSummaryResponse
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class ApartmentController(val service: ApartmentService) {

    @PostMapping("add")
    @ResponseStatus(HttpStatus.CREATED)
    fun addApartment(@RequestBody request: AddApartmentsRequest) {
        service.addApartments(request.buildingId, request.apartments)
    }

    @GetMapping("my")
    @ResponseStatus(HttpStatus.OK)
    fun getMyApartments(@RequestHeader("x-user-email") email: String): MyApartmentsSummaryResponse {
        print(email)
        val myApartmentsSummaryResponse = MyApartmentsSummaryResponse(service.userApartments(email))
        return myApartmentsSummaryResponse
    }
}