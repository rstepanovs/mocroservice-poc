package lv.romans.master.apartment.service

data class AddApartmentsRequest(val buildingId: Long, val apartments: List<ApartmentDto>)