package lv.romans.master.apartment.ext

import feign.Param
import feign.QueryMap
import feign.RequestLine
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.data.jpa.repository.Query
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam

@FeignClient("building-service")
interface BuildingApi {

    @RequestMapping(method = [RequestMethod.GET], path = ["internal/addresses"])
    fun listBuildingAddresses(@RequestParam("buildings") ids: List<Long>): BuildingAddressResponse

    data class BuildingAddressResponse(val addresses: List<BuildingAddress>)

    data class BuildingAddress(val id: Long, val city: String, val street: String, val streetNum: Int)
}