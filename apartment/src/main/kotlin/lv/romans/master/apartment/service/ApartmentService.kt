package lv.romans.master.apartment.service

import feign.FeignException
import lv.romans.master.apartment.ext.BuildingApi
import lv.romans.master.apartment.repo.Apartment
import lv.romans.master.apartment.repo.ApartmentRepository
import org.springframework.stereotype.Service

@Service
class ApartmentService(val repository: ApartmentRepository, val buildingApi: BuildingApi) {

    fun addApartments(buildingId: Long, apartments: List<ApartmentDto>) {
        apartments.map { Apartment(0, buildingId, it.number, it.ownerEmail) }.forEach { repository.save(it) }
    }

    fun userApartments(email: String): List<ApartmentInfoDto> {
        val apartments = repository.findByOwnerEmail(email)
        val buildings = try {
            apartments.map { it.buildingId }.toSet().let {
                buildingApi.listBuildingAddresses(it.toList())
            }
                    .addresses.map { it.id to AddressDto(it.city, it.street, it.streetNum) }.toMap().also { print(it) }
        } catch (e: FeignException) {
            e.printStackTrace()
            emptyMap<Long, AddressDto>()
        }
        return apartments.map { ApartmentInfoDto(it.number, buildings[it.buildingId]) }
    }
}