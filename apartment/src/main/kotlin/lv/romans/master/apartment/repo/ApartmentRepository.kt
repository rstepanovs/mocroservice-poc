package lv.romans.master.apartment.repo

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.validation.constraints.Email

@Repository
interface ApartmentRepository: CrudRepository<Apartment, Long> {
    fun findByOwnerEmail(email: String): List<Apartment>
}