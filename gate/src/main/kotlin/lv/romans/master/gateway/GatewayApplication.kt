package lv.romans.master.gateway

import org.bouncycastle.util.encoders.Base64
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.zuul.EnableZuulProxy
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import java.security.KeyFactory
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec

@EnableDiscoveryClient
@EnableZuulProxy
@SpringBootApplication
class GatewayApplication {

    @Bean
    fun privateKey(): RSAPublicKey {
        val publicKey = ClassPathResource("security/public.pem").file.readText()
                .replace("-----BEGIN PUBLIC KEY-----\n", "")
                .replace("\n-----END PUBLIC KEY-----", "")
                .run { Base64.decode(this) }
        val keySpec = X509EncodedKeySpec(publicKey)

        return KeyFactory.getInstance("RSA").generatePublic(keySpec) as RSAPublicKey
    }

}

fun main(args: Array<String>) {
    runApplication<GatewayApplication>(*args)
}

@EnableWebSecurity
class SecurityConfiguration(val publicKey: RSAPublicKey): WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers("/api/auth/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(JWTAuthorizationFilter(authenticationManager(), publicKey))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
