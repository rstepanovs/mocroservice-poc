package lv.romans.master.gateway

import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class AuthHeaderFilter : ZuulFilter() {
    override fun filterType(): String {
        return FilterConstants.PRE_TYPE
    }

    override fun filterOrder(): Int {
        return FilterConstants.SIMPLE_HOST_ROUTING_FILTER_ORDER - 1
    }

    override fun shouldFilter(): Boolean {
        val requestContext = RequestContext.getCurrentContext()
        val authentication = SecurityContextHolder.getContext().authentication
        return authentication != null && authentication.principal != null
    }

    override fun run() {
        val requestContext = RequestContext.getCurrentContext()
        val authentication = SecurityContextHolder.getContext().authentication
        val email = authentication.principal as String
        val role = authentication.authorities.first().authority
        requestContext.addZuulRequestHeader(USER_HEADER, email)
        requestContext.addZuulRequestHeader(USER_ROLE, role)
    }

    companion object {
        const val USER_HEADER = "x-user-email"
        const val USER_ROLE = "x-user-role"
    }
}